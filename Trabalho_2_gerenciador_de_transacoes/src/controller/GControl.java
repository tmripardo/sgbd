package controller;

import java.util.Iterator;
import java.util.Random;

import javax.swing.JOptionPane;

import view.GUI;
import view.Quadro;
import model.*;

/**
 * Controller principal da nossa aplicacao aqui eh onde ocorrem todas as alteracoes no grafo.
 * Exemplos: Adicionar, Remover.
 * 
 * @author Bruno Mourao, Thiago Ripardo.
 * @version 1.1
 */

public class GControl {
	
	/**
	 * Atributos
	 */
	
	private Grafo G = null;
	private  GUI frameDeControle;
	private Quadro pane = null;

	/**
	 * Construtor GControl
	 * @param G
	 * @since 1.0 
	 */
	public GControl(Grafo G){
		this.G = G;
	}

	/**
	 * Determina se o grafo eh ou nao direcionado.
	 * @param dir boolean
	 * @since 1.0 
	 */
	public void addDir(boolean dir){
		G.setDir(dir);
	}

	/**
	 * Adiciona um novo vertice de acordo com seu nome e posicao.
	 * @param nome String
	 * @param x int
	 * @param y int
	 * @since 1.0 
	 */
	public void addVertice(String nome, int x, int y) {
		Vertice u = null;
		try {
			u = G.getVertice(nome);
		}
		catch (NullPointerException e) {
			G.getV().add(new Vertice(nome, x, y));
			//JOptionPane.showMessageDialog(frameDeControle,"Vertice inserido com sucesso!","Yeah! :D",JOptionPane.INFORMATION_MESSAGE);
		}
		finally {
			if(u!=null) 
				JOptionPane.showMessageDialog(frameDeControle,"O vertice inserido ja existe!","Ops! :(",JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Adiciona vertice v
	 * @param v model.Vertice
	 * @since 1.0 
	 */
	public void addVertice(Vertice v) {
		G.getV().add(v);
	}

	/**
	 * Adiciona a aresta e.
	 * @param e model.Aresta
	 * @since 1.0 
	 */
	public void addAresta(Aresta e) {
		G.getE().add(e);
	}

	/**
	 * Adiciona aresta para dois vertices conhecidos.
	 * @param u model.Vertice
	 * @param v model.Vertice
	 * @since 1.0 
	 */
	public void addAresta(Vertice u, Vertice v) {
		G.getE().add(new Aresta(u, v));
	}

	/**
	 * Adiciona aresta de acordo com o nome de dois vertices e seu peso
	 * @param nome1 String
	 * @param nome2 String
	 * @param peso int
	 * @since 1.0 
	 */
	public void addAresta(String nome1, String nome2, int peso) {
		Aresta e = null;
		try {
			e = G.getAresta(nome1, nome2);
		}
		catch(NullPointerException ex){
			Vertice u = null;
			Vertice v = null;
			try{
				u = G.getVertice(nome1);
				v = G.getVertice(nome2);
			}
			catch(NullPointerException ex2){
				JOptionPane.showMessageDialog(frameDeControle,"Pelo menos um dos vertices inseridos nao existe, nao poderemos prosseguir. Tente novamente com vertices validos.","Ops! :(",JOptionPane.ERROR_MESSAGE);
			}
			if((u!=null)&&(v!=null)){
				G.getE().add(new Aresta(u, v, peso));
				//JOptionPane.showMessageDialog(frameDeControle,"Aresta inserida com sucesso!","Yeah! :D",JOptionPane.INFORMATION_MESSAGE);
			}
		}
		finally{
			if(e!=null) 
				JOptionPane.showMessageDialog(frameDeControle,"A aresta inserida ja existe!","Ops! :(",JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Remove vertice de acordo com seu nome
	 * @param nome String
	 * @since 1.0 
	 */
	public void removerVertice(String nome) {
		Vertice u = null;
		try{
			u = G.getVertice(nome);
			Iterator<Aresta> iter = G.getE().iterator();
			Aresta e = null;

			while (iter.hasNext()){
				e = iter.next();
				if(e.containsU(u)||e.containsV(u)){
					iter.remove();
					G.getE().remove(e);

				}
			}
			G.getV().remove(u);
			//JOptionPane.showMessageDialog(frameDeControle,"Vertice removido com sucesso!","Yeah! :D",JOptionPane.INFORMATION_MESSAGE);
		}
		catch(NullPointerException e){
			JOptionPane.showMessageDialog(frameDeControle,"O vertice inserido nao existe!","Ops! :(",JOptionPane.ERROR_MESSAGE);
		}

	}

	/**
	 * Remove aresta de acordo com o nome de seus vertices
	 * @param nome1 String
	 * @param nome2 String
	 * @since 1.0 
	 */
	public void removerAresta(String nome1, String nome2) {
		Aresta e = null;
		try{

			e = G.getAresta(nome1,nome2);
			G.getE().remove(e);
			//JOptionPane.showMessageDialog(frameDeControle,"Aresta removida com sucesso!","Yeah! :D",JOptionPane.INFORMATION_MESSAGE);

		}
		catch (NullPointerException ex){
			JOptionPane.showMessageDialog(frameDeControle,"A aresta inserida nao existe!","Ops! :(",JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Reseta estado atual do grafo para o estado original no qual foi criado.
	 * @since 1.0 
	 */
	public void resetarEstado(){
		Iterator<Vertice> iter = G.getV().iterator();
		Vertice u = null;
		while (iter.hasNext()){
			u = iter.next();
			u.setCor(null);
			u.setD(null);
			u.setF(null);
			u.getFigura().setCor("dgray");
			u.getFigura().setD(null);
			u.getFigura().setF(null);
		}
	}

	/**
	 * Agrupa todos os vertices em uma determinada posicao
	 * @param x int
	 * @param y int
	 * @since 1.0 
	 */
	public void agrupar(int x, int y){
		Iterator<Vertice> iter = G.getV().iterator();
		Vertice u = null;
		if(iter.hasNext()){
			while (iter.hasNext()){
				u = iter.next();
				u.setX(x);
				u.setY(y);
				u.getFigura().setX(x);
				u.getFigura().setY(y);
				//pane.repaint();
			}
		}
		else{
			//JOptionPane.showMessageDialog(frameDeControle,"Nao ha vertices para serem agrupados!","Ops! :(",JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Ajusta um determinado vertice a uma posicao escolhida.
	 * @param nome String
	 * @param x int
	 * @param y int
	 * @since 1.0 
	 */
	public void ajustar(String nome, int x, int y){
		try{
			Vertice u = G.getVertice(nome);
			u.setX(x);
			u.setY(y);
			u.getFigura().setX(x);
			u.getFigura().setY(y);
		}
		catch (NullPointerException ex){
			JOptionPane.showMessageDialog(frameDeControle,"O vertice inserido não existe!","Ops! :(",JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Seta qual model.Grafo ira ser usado pela classe
	 * @param G model.Grafo
	 * @since 1.0 
	 */
	public void setGrafo(Grafo G){
		this.G = G;
	}

	/**
	 * Seta qual o view.Quadro vai ser usado pela classe
	 * @param pane model.Quadro
	 * @since 1.0 
	 */
	public void setQuadro(Quadro pane){
		this.pane = pane;
	}

	/**
	 * Seta uma transacao em um determinado vertice
	 * @param id String
	 * @param status String
	 * @param u model.Vertice
	 * @since 1.1
	 */
	public void setTransacao(String id, String status, Vertice u){
		Transacao t2 = null;
		try {
			t2 = u.getTransacao(id);
		}
		catch (NullPointerException e) {
			u.getListaTransacoes().add(new Transacao(id, status));
		}
		finally {
			if(t2!=null) 
				JOptionPane.showMessageDialog(frameDeControle,"O vertice inserido ja existe!","Ops! :(",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Remove uma transacao em um determinado vertice
	 * @param id String
	 * @param u model.Vertice
	 * @since 1.1 
	 */
	public void remTransacao(String id, Vertice u){
		u.getListaTransacoes().remove(u.getTransacao(id));
	}
	
	/**
	 * Remove uma transacao em qualquer vertice que ela esteja
	 * @param id String
	 * @since 1.1 
	 */
	public void remTransacao(String id){
		Iterator<Vertice> iter = G.getV().iterator();
		Vertice u = null;
		Transacao t = null;
		while (iter.hasNext()){
			
			u = iter.next();
			try{
				t = u.getTransacao(id);
			}
			catch(NullPointerException e){}
			finally{
				if(t!=null){
					u.getListaTransacoes().remove(u.getTransacao(id));
					break;
				}
			}
		}
	}
	
	/**
	 * Modifica o status de uma transacao
	 * @param id String
	 * @param status String
	 * @since 1.1 
	 */
	public void modificarStatus(String id, String status){
		if(status.equals("READ")||status.equals("WRITE")||status.equals("TR_Terminate")||status.equals("TR_Rollback")||status.equals("TR_Commit")||status.equals("TR_Finish")){
			
			Iterator<Vertice> iter = G.getV().iterator();
			Vertice u = null;
			Transacao t = null;
			
			while (iter.hasNext()){
				
				u = iter.next();
				try{
					t = u.getTransacao(id);
				}
				
				catch(NullPointerException e){}
				if(t!=null){
					Iterator<Aresta> iter2 = G.getE().iterator();
					Aresta e = null;
					while (iter2.hasNext()){
						e = iter2.next();
						Vertice v = null;

						if(e.containsU(u)){
							v = e.getV();
						}
						try{
							if(status.equals("READ")){
								if(v.getNome().equals("Ativa")){
									//System.out.println(v.getNome());
									remTransacao(id,u);
									setTransacao(id,status,v);
								}
							}
							else if(status.equals("WRITE")){
								if(v.getNome().equals("Ativa")){
									remTransacao(id,u);
									setTransacao(id,status,v);
								}
							}
							else if(status.equals("TR_Terminate")){
								if(v.getNome().equals("Processo_Efetivacao")){
									remTransacao(id,u);
									setTransacao(id,status,v);
								}
							}
							else if(status.equals("TR_Rollback")){
								if(v.getNome().equals("Processo_Cancelamento")){
									remTransacao(id,u);
									setTransacao(id,status,v);
								}
							}
							else if(status.equals("TR_Commit")){
								if(v.getNome().equals("Efetivada")){
									remTransacao(id,u);
									setTransacao(id,status,v);
								}
							}
							else if(status.equals("TR_Finish")){
								if(v.getNome().equals("TR_Finalizada")){
									remTransacao(id,u);
									setTransacao(id,status,v);
								}
							}
						}
						catch(NullPointerException ex){}
					}
				}
				else{
					
				}
			}
			
		}
		else{
			JOptionPane.showMessageDialog(frameDeControle,"Comando Errado!","Ops! :(",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Retorna o Grafo que esta instanciado em GControl
	 * @deprecated
	 * @return G <code>model.Grafo</code>
	 * @since 1.0 
	 */
	public Grafo getGrafo(){
		return this.G;
	}

	/**
	 * Instancia novo grafo
	 * @deprecated
	 * @since 1.0 
	 */
	public void novoGrafo(){
		G = new Grafo();
	}

}