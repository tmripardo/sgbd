package view;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.util.Iterator;
import javax.swing.Timer;
import model.Aresta;
import model.Grafo;
import model.Vertice;

/**
 * Classe Quadro, onde sao feitos os desenhos e onde sao capturados os movimentos do mouse.
 * 
 * @author Thiago Ripardo.
 * @version 1.0
 */
public class Quadro extends JPanel {

	private static final long serialVersionUID = 1L;
	private final int DELAY = 5;
	private Timer timer;
	private Grafo G = null;

	/**
	 * Construtor da classe Quadro
	 * 
	 * @param G Grafo
	 */
	
	public Quadro(Grafo G) {

		super(new BorderLayout());
		this.G = G;
	}

	public void setGrafo(Grafo G){
		this.G = G;
	}
	
	/**
	 * Metodo que pinta os componentes
	 * 
	 * @param g Graphics
	 * @see model.FiguraAresta
	 * @see model.FiguraVertice
	 * @since 1.0
	 */
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		if(G.getDir()){
			Iterator<Aresta> iter = G.getE().iterator();
			Aresta e = null;
			while (iter.hasNext()){
				e = iter.next();
				if(e.getU().equals(e.getV()))
					e.getFigura().desenhandoElipse(g);
				else
					e.getFigura().desenhandoSeta(g);
			}
		}
		else{
			Iterator<Aresta> iter = G.getE().iterator();
			Aresta e = null;
			while (iter.hasNext()){
				e = iter.next();
				if(e.getU().equals(e.getV()))
					e.getFigura().desenhandoElipse(g);
				else
					e.getFigura().desenhandoLinha(g);
			}
		}

		Iterator<Vertice> iter2 = G.getV().iterator();
		while (iter2.hasNext())
			iter2.next().getFigura().desenhandoCirculo(g);
	}
}
