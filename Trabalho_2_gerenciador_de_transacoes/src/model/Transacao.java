package model;

import java.io.Serializable;

/**
 * Classe que representa uma transacao em determinado vertice.
 * 
 * @author Bruno Mourao, Thiago Ripardo.
 * @version 1.0
 */
public class Transacao implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id, status;
	
	/**
	 * Construtor Transacao
	 * @since 1.0 
	 */
	public Transacao(){
		
	}
	
	/**
	 * Construtor Transacao
	 * @param id String
	 * @param status String
	 * @since 1.0 
	 */
	public Transacao(String id, String status){
		this.id = id;
		this.status = status;
	}
	
	/**
	 * Retornar id da transacao
	 * @return id String
	 * @since 1.0 
	 */
	public String getID(){
		return this.id;
	}
	
	/**
	 * Retornar status da transacao
	 * @return status String
	 * @since 1.0 
	 */
	public String getStatus(){
		return this.status;
	}
}
