package view;

import javax.swing.*;

import model.Grafo;
import model.Item;
import model.Vertice;
import controller.*;
import java.awt.*;
import java.awt.event.*;


/**
 * Esta eh a janela principal do programa, onde estao as principais acoes. 
 * Como criar um novo grafo, inserir vertices e arestas, transacoes e muitas outras (Listeners).
 * 
 * @author Bruno Mourão, Thiago Ripardo.
 * @version 1.1
 */

public final class GUIGrafo extends GUI {

	private static final long serialVersionUID = 1L;
	private GUI frameDeControle;
	private Grafo G = new Grafo();
	private Tr_Manager gc = new Tr_Manager(G);
	private final Quadro pane2 = new Quadro(G);
	private static JTextArea aTextArea = new JTextArea();
	
	/**
	 * Construtor da classe GUIGrafo
	 */
	
	public GUIGrafo(){

		super("Gerenciador de Transacoes");
		iniciar();
		iniciarVerticesArestasEItens();
	}

	/**
	 * Inicia os objetos como: paineis, menus, menuitens, barras de menu, etc.
	 */
	
	public void iniciar(){

		// Instanciar Paineis
		JPanel pane = new JPanel(new BorderLayout());
		gc.setQuadro(pane2);
		pane.setBorder(BorderFactory.createLineBorder(Color.black, 2));

		// Icone da GUIGrafo(frame)
		Image imagemTitulo = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("images/graph.png"));  
		this.setIconImage(imagemTitulo);

		// Instanciar e inserir icones dos JMenus
		JMenu menuArquivo = new JMenu("Arquivo");
		ImageIcon arq = new ImageIcon(this.getClass().getResource("images/arquivo/folder.png"));
		menuArquivo.setIcon(arq);
		JMenu menuEditar = new JMenu("Editar");
		ImageIcon edit = new ImageIcon(this.getClass().getResource("images/editar/application_edit.png"));
		menuEditar.setIcon(edit);
		JMenu menuInfo = new JMenu("Informacoes");
		ImageIcon info = new ImageIcon(this.getClass().getResource("images/informacoes/information.png"));
		menuInfo.setIcon(info);
		JMenu menuAjuda = new JMenu("Ajuda");
		ImageIcon aju = new ImageIcon(this.getClass().getResource("images/ajuda/help.png"));
		menuAjuda.setIcon(aju);

		// Instanciar e inserir icones dos JMenuItens e JCheckBoxMenuItem
		ImageIcon abrir = new ImageIcon(this.getClass().getResource("images/arquivo/folder_explore.png"));
		JMenuItem itemAbrir = new JMenuItem("Abrir", abrir);

		ImageIcon salvar = new ImageIcon(this.getClass().getResource("images/arquivo/page_save.png"));
		JMenuItem itemSalvar = new JMenuItem("Salvar", salvar);

		ImageIcon sair = new ImageIcon(this.getClass().getResource("images/arquivo/cross.png"));
		JMenuItem itemSair = new JMenuItem("Sair", sair);

		ImageIcon iAjustar = new ImageIcon(this.getClass().getResource("images/editar/application_edit.png"));
		JMenuItem itemAjustar = new JMenuItem("Ajustar Vértice", iAjustar);

		ImageIcon iAgrupar = new ImageIcon(this.getClass().getResource("images/editar/application_edit.png"));
		JMenuItem itemAgrupar = new JMenuItem("Agrupar Vértices", iAgrupar);

		ImageIcon vi = new ImageIcon(this.getClass().getResource("images/editar/add.png"));

		ImageIcon vr = new ImageIcon(this.getClass().getResource("images/editar/delete.png"));
		
		ImageIcon ed = new ImageIcon(this.getClass().getResource("images/editar/application_edit.png"));	

		ImageIcon geral = new ImageIcon(this.getClass().getResource("images/informacoes/information.png"));
		JMenuItem itemGeral = new JMenuItem("Geral", geral);

		ImageIcon vesp = new ImageIcon(this.getClass().getResource("images/informacoes/zoom.png"));
		JMenuItem itemVEsp = new JMenuItem("Vertice (Especifico)", vesp);

		ImageIcon icv = new ImageIcon(this.getClass().getResource("images/informacoes/information.png"));
		JMenuItem itemCV = new JMenuItem("Conjunto de Vertices", icv);

		ImageIcon ica = new ImageIcon(this.getClass().getResource("images/informacoes/information.png"));
		JMenuItem itemCA = new JMenuItem("Conjunto de Arestas", ica);

		ImageIcon vatual = new ImageIcon(this.getClass().getResource("images/ajuda/world.png"));
		JMenuItem itemVAtual = new JMenuItem("Verificar atualizacoes", vatual);

		ImageIcon sobre = new ImageIcon(this.getClass().getResource("images/ajuda/information.png"));
		JMenuItem itemSobre = new JMenuItem("Sobre", sobre);

		// Instanciar JMenuBar
		JMenuBar menuBar = new JMenuBar();

		// Barra de ferramentas
		JToolBar toolBar = new JToolBar("Barra de Ferramentas");
		JLabel vert = new JLabel("Transacoes:");

		// Botao adicionar transacao
		JButton addV = new JButton();
		addV.setIcon(vi);
		addV.setOpaque(false);
		addV.addActionListener(new HandlerAddHistoria());

		// Botao remover transacao
		JButton remV = new JButton();
		remV.setIcon(vr);
		remV.setOpaque(false);
		remV.addActionListener(new HandlerRemTransacao());
		
		// Botao modificar transacao
		JButton mod = new JButton();
		mod.setIcon(ed);
		mod.setOpaque(false);
		mod.addActionListener(new HandlerModTransacao());

		// Botao procurar vertice
		JButton proV = new JButton();
		proV.setIcon(vesp);
		proV.setOpaque(false);
		proV.addActionListener(new HandlerVerticeEsp());

		//Setar na toolbar
		toolBar.add(vert);
		toolBar.add(addV);
		toolBar.add(remV);
		toolBar.add(mod);
		toolBar.add(proV);
		

		// Secao de adicionar aos Menus: MenuItens ou outros Menus. Tambem tratamos eventos aqui.
		// Menu Arquivo
		menuArquivo.setMnemonic(KeyEvent.VK_W);

		// Abrir
		itemAbrir.addActionListener(new HandlerAbrir());
		menuArquivo.add(itemAbrir);

		// Salvar
		itemSalvar.addActionListener(new HandlerSalvar());
		menuArquivo.add(itemSalvar);

		// Sair
		itemSair.setMnemonic(KeyEvent.VK_C);
		itemSair.addActionListener(new HandlerSair());
		menuArquivo.add(itemSair);

		// Menu Editar
		// Ajustar
		itemAjustar.addActionListener(new HandlerAjustar());
		// Agrupar
		itemAgrupar.addActionListener(new HandlerAgrupar());
		menuEditar.setMnemonic(KeyEvent.VK_W);
		menuEditar.addSeparator();
		menuEditar.add(itemAjustar);
		menuEditar.add(itemAgrupar);
		menuEditar.addSeparator();

		// Menu Informacoes
		menuInfo.setMnemonic(KeyEvent.VK_W);

		// Menu Geral
		itemGeral.setMnemonic(KeyEvent.VK_C);
		itemGeral.addActionListener(new HandlerGeral());
		menuInfo.add(itemGeral);

		menuInfo.addSeparator();

		// Vertice Especifico
		itemVEsp.setMnemonic(KeyEvent.VK_C);
		itemVEsp.addActionListener(new HandlerVerticeEsp());
		menuInfo.add(itemVEsp);

		// Conjunto de Vertices
		itemCV.setMnemonic(KeyEvent.VK_C);
		itemCV.addActionListener(new HandlerCV());
		menuInfo.add(itemCV);

		// Conjunto de Arestas
		itemCA.setMnemonic(KeyEvent.VK_C);
		itemCA.addActionListener(new HandlerCA());
		menuInfo.add(itemCA);

		// Menu Ajuda
		menuAjuda.setMnemonic(KeyEvent.VK_W);

		// Verificar Atualizacoes
		itemVAtual.setMnemonic(KeyEvent.VK_C);
		itemVAtual.addActionListener(new HandlerVAtual());
		menuAjuda.add(itemVAtual);

		menuAjuda.addSeparator();

		// Sobre
		itemSobre.setMnemonic(KeyEvent.VK_C);
		itemSobre.addActionListener(new HandlerSobre());
		menuAjuda.add(itemSobre);

		menuBar.add(menuArquivo);
		menuBar.add(menuEditar);
		menuBar.add(menuInfo);
		menuBar.add(menuAjuda);

		// Setar objetos e configurações
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {}
		});
		
		setJMenuBar(menuBar);
		toolBar.setBackground(new Color(220,220,220));
		toolBar.setBorder(BorderFactory.createLineBorder(Color.gray, 1));
		pane2.setBackground(new Color(220,220,220));
		//pane2.add(new JScrollPane(aTextArea), BorderLayout.PAGE_END);
		pane.add(toolBar, BorderLayout.PAGE_START);
		//pane.add(pane2, BorderLayout.CENTER);
		//JTextArea aTextArea = new JTextArea();
		
		//aTextArea.append("Teste\n");
		//pane.add(pane2, BorderLayout.CENTER);
		pane.add(new JScrollPane(aTextArea), BorderLayout.CENTER);
		
		//add("Center", new JScrollPane(aTextArea));
		setContentPane(pane);
		setSize(600,400);
		
		
	}
	
	/**
	 * Metodo que inicia vertices e arestas padroes
	 * @since 1.1 
	 */
	public void iniciarVerticesArestasEItens(){
		
		gc.addVertice("TR_Iniciada", 44, 152);
		gc.addVertice("Ativa", 173, 153);
		gc.addVertice("Processo_Cancelamento", 360, 97);
		gc.addVertice("Processo_Efetivacao", 360, 235);
		gc.addVertice("Efetivada", 568, 237);
		gc.addVertice("TR_Finalizada", 741, 102);
		
		gc.addAresta("TR_Iniciada", "Ativa", 0);
		gc.addAresta("Ativa", "Ativa", 0);
		gc.addAresta("Ativa", "Processo_Efetivacao", 0);
		gc.addAresta("Ativa", "Processo_Cancelamento", 0);
		gc.addAresta("Processo_Efetivacao", "Efetivada", 0);
		gc.addAresta("Processo_Efetivacao", "Processo_Cancelamento", 0);
		gc.addAresta("Efetivada", "TR_Finalizada", 0);
		gc.addAresta("Processo_Cancelamento", "TR_Finalizada", 0);
		
		gc.addDir(true);
		
		gc.setItem("x", 2);
		gc.setItem("y", 50);
		gc.setItem("z", 950);
		
		
		
	}
	
	/**
	 * Seta qual model.Grafo ira ser usado pela classe
	 * @param G model.Grafo
	 * @since 1.0 
	 */
	public void setGrafo(Grafo G){
		this.G = G;
	}
	
	/**
	 * Seta uma atualizacao de JTextArea
	 * @param s String
	 * @since 1.2 
	 */
	public static void atualizarText(String s){
		aTextArea.append(s+"\n");
	}
	
	/**
	 * Classe privada que trata eventos do JMenuItem Abrir.
	 * @version 1.0
	 * @since 1.0 
	 */
	private class HandlerAbrir implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			try{
				AbrindoGrafo ag = new AbrindoGrafo();
				setGrafo(ag.iniciar());
				gc.setGrafo(G);
				pane2.setGrafo(G);
				repaint();
			}
			catch(NullPointerException ex){}
		}
	}

	/**
	 * Classe privada que trata eventos do JMenuItem Salvar.
	 * @version 1.0
	 * @since 1.0 
	 */
	private class HandlerSalvar implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			(new SalvandoGrafo(G)).iniciar();
		}
	}

	/**
	 * Classe privada que trata eventos do JMenuItem Sair.
	 * @version 1.0
	 * @since 1.0 
	 */
	private class HandlerSair implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			Object[] message = {"Deseja fechar o programa?\nVoce tem certeza disso?"};
			int option = JOptionPane.showOptionDialog(frameDeControle, message, "Sair", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE, null, new String[]{"Sim", "Nao"}, "Nao");
			if(option == JOptionPane.YES_OPTION){
				setVisible(false);
				dispose();
			}
		}
	}

	/**
	 * Classe privada que trata eventos do JMenuItem Agrupar.
	 * @version 1.0
	 * @since 1.0 
	 */
	private class HandlerAgrupar implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			JTextField nome1 = new JTextField();
			JTextField nome2 = new JTextField();

			Object[] message = {
					"Posicao X:", nome1,
					"Posicao Y:", nome2
			};		
			int option = JOptionPane.showConfirmDialog(frameDeControle, message, "Agrupar Vertices", JOptionPane.OK_CANCEL_OPTION);
			if(option == JOptionPane.OK_OPTION){
				try{
					gc.agrupar(Integer.parseInt(nome1.getText()), Integer.parseInt(nome2.getText()));
					repaint();
				}
				catch(NumberFormatException ex) {
					JOptionPane.showMessageDialog(frameDeControle,"As posicoes estao incorretas. Tente novamente com posicoes validas","Ops! :(",JOptionPane.ERROR_MESSAGE);
				}
			}
		}

	}

	/**
	 * Classe privada que trata eventos do JMenuItem Ajustar.
	 * @version 1.0
	 * @since 1.0 
	 */
	private class HandlerAjustar implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			JTextField nome = new JTextField();
			JTextField nome1 = new JTextField();
			JTextField nome2 = new JTextField();

			Object[] message = {
					"Nome:", nome,
					"Posicao X:", nome1,
					"Posicao Y:", nome2
			};		
			int option = JOptionPane.showConfirmDialog(frameDeControle, message, "Ajustar Posição de Vértice", JOptionPane.OK_CANCEL_OPTION);
			if(option == JOptionPane.OK_OPTION){
				try{
					gc.ajustar(nome.getText(), Integer.parseInt(nome1.getText()), Integer.parseInt(nome2.getText()));
					repaint();
				}
				catch(NumberFormatException ex) {
					JOptionPane.showMessageDialog(frameDeControle,"As posicoes estao incorretas. Tente novamente com posicoes validas","Ops! :(",JOptionPane.ERROR_MESSAGE);
				}
			}
		}

	}
	/**
	 * Classe privada que trata eventos do JMenuItem AddHistoria.
	 * @version 1.2
	 * @since 1.2 
	 */
	private class HandlerAddHistoria implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent e) {
			String id = JOptionPane.showInputDialog(frameDeControle,"Entre com uma historia:","Inserir historia", JOptionPane.QUESTION_MESSAGE);
			try{
				//while(id.equals(""))
					//id = JOptionPane.showInputDialog(frameDeControle,"\nUma historia nao pode ser nula, certo?\n\nInsira a historia:","Que feio! :/",JOptionPane.QUESTION_MESSAGE);
				gc.tratadorDeEventos(new TratadorDeEntradas().getListadeComandos(id));
				
			}
			catch(NullPointerException ex){}

		}
	}
	
	/**
	 * Classe privada que trata eventos do JMenuItem AddTransacao.
	 * @version 1.1
	 * @since 1.1 
	 */
	private class HandlerAddTransacao implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent e) {
			String id = JOptionPane.showInputDialog(frameDeControle,"Entre com o nome da transacao:","Inserir Transacao", JOptionPane.QUESTION_MESSAGE);
			try{
				while(id.equals(""))
					id = JOptionPane.showInputDialog(frameDeControle,"\nUma transacao nao pode ser nula, certo?\n\nInsira o id da transacao:","Que feio! :/",JOptionPane.QUESTION_MESSAGE);
				gc.setTransacao(id, "TR_Begin", G.getVertice("TR_Iniciada"));
				repaint();
			}
			catch(NullPointerException ex){}

		}
	}

	/**
	 * Classe privada que trata eventos do JMenuItem RemVertice.
	 * @since 1.0 
	 */
	private class HandlerRemTransacao implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			String id = JOptionPane.showInputDialog(frameDeControle,"Entre com o id da transacao:","Remover Transacao", JOptionPane.QUESTION_MESSAGE);
			try{
				while(id.equals(""))
					id = JOptionPane.showInputDialog(frameDeControle,"\nUma transacao nao pode ser nula, certo?\n\nInsira o id da transacao:","Que feio! :/",JOptionPane.QUESTION_MESSAGE);
				gc.remTransacao(id);
				repaint();
			}
			catch(NullPointerException ex){}

		}

	}

	/**
	 * Classe privada que trata eventos do JButton mod.
	 * @since 1.1 
	 */
	private class HandlerModTransacao implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			/*JTextField id = new JTextField();
			JTextField status = new JTextField();
			//JTextField nome3 = new JTextField();

			Object[] message = {
					"ID:", id,
					"Novo Status:", status
			};		
			int option = JOptionPane.showConfirmDialog(frameDeControle, message, "Modificar Transacao", JOptionPane.OK_CANCEL_OPTION);
			if(option == JOptionPane.OK_OPTION){
				gc.modificarStatus(id.getText(), status.getText());
				repaint();
			}*/
		}

	}

	/**
	 * Classe privada que trata eventos do JMenuItem Geral.
	 * @version 1.0
	 * @since 1.0 
	 */
	private class HandlerGeral implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			String nome = null;
			if(G.getDir())
				nome = "Direcionado.";
			else
				nome = "Não direcionado.";
			JOptionPane.showMessageDialog(frameDeControle, "Vertices:"+ G.getV().size()+ "\n Arestas:" +G.getE().size()+ "\n" +nome,"Geral", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	/**
	 * Classe privada que trata eventos do JMenuItem VerticeEspecifico.
	 * @version 1.0
	 * @since 1.0 
	 */
	private class HandlerVerticeEsp implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			JTextField nome1 = new JTextField();

			Object[] message = {"Entre com o nome do vertice", nome1};

			int option = JOptionPane.showConfirmDialog(frameDeControle, message, "Vertice (Especifico)", JOptionPane.OK_CANCEL_OPTION);
			if(option == JOptionPane.OK_OPTION){
				try {
					JOptionPane.showMessageDialog(frameDeControle, "Nome: "+G.getVertice(nome1.getText()).getNome()+"\nTransacoes:\n"+G.getVertice(nome1.getText()).getInfoTransacoes()+"\n","Vertice (Especifico)", JOptionPane.INFORMATION_MESSAGE);
				}
				catch(NullPointerException ex2) {
					JOptionPane.showMessageDialog(frameDeControle,"O vertice inserido nao existe!","Ops! :(",JOptionPane.ERROR_MESSAGE);
				}

			}
		}
	}

	/**
	 * Classe privada que trata eventos do JMenuItem Conjunto de Vertices.
	 * @version 1.0
	 * @since 1.0 
	 */
	private class HandlerCV implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			JOptionPane.showMessageDialog(frameDeControle, "Vertices: "+G.getInfoVertices()+ "\nQuantidade: "+ G.getV().size() ,"Conjunto de Vertices", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	/**
	 * Classe privada que trata eventos do JMenuItem Conjunto de Arestas.
	 * @version 1.0
	 * @since 1.0 
	 */
	private class HandlerCA implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {

			JOptionPane.showMessageDialog(frameDeControle,"Arestas: " + G.getInfoArestas() + "\nQuantidade: " + G.getE().size()  ,"Conjunto de Arestas", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	/**
	 * Classe privada que trata eventos do JMenuItem Verificar atualizacoes.
	 * @version 1.0
	 * @since 1.0 
	 */
	private class HandlerVAtual implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(frameDeControle, "Nao foi possivel verificar novas atualizacoes.","Ops! :(", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Classe privada que trata eventos do JMenuItem Sobre.
	 * @version 1.0
	 * @since 1.0 
	 */
	private class HandlerSobre implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(frameDeControle, "Desenvolvido por: \n\nBruno Mourao de Lavor Moreira;\nThiago Mendes Ripardo Aguiar.\n\n","Sobre", JOptionPane.INFORMATION_MESSAGE);
		}
	}
}


