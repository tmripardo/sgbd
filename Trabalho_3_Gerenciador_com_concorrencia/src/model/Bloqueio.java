package model;

public class Bloqueio {
	
	public static final int DESBLOQUEADO = 10;
	public static final int BLOQUEADO_LEITURA = 11;
	public static final int BLOQUEADO_ESCRITA = 12;
	public static final int BLOQUEIO_INVALIDO = 13;
	public static final int ITEM_NAO_USADO = -1;
	
	private int ts;
	private int estado;
	private Transacao trBloqueante = null;
	
	public int getTS() {
		return ts;
	}
	public void setTS(int ts) {
		this.ts = ts;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	
	public void setTransacaoBloqueante(Transacao tr){
		this.trBloqueante = tr;
	}
	public Transacao getTransacaoBloqueante(){
		return this.trBloqueante;
	}
	
}
