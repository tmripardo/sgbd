package model;

public class Wait_Q {
	private Fila<Integer> filaTipoDeBloqueio = new Fila<>();
	private Fila<Transacao> filaDeTransacoes = new Fila<>();
	
	public void inserir(Transacao tr, int tipoDeBloqueio){
		this.filaTipoDeBloqueio.add(tipoDeBloqueio);
		this.filaDeTransacoes.add(tr);
	}
	
	public Transacao getProximaTransacao(){
		return this.filaDeTransacoes.peek();
	}
	
	public int getProximoTipoDeBloqueio(){
		return this.filaTipoDeBloqueio.peek();
	}
	
	public void remover(){
		this.filaTipoDeBloqueio.remove();
		this.filaDeTransacoes.remove();
	}
}
