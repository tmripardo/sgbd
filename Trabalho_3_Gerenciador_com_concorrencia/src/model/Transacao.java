package model;

import java.io.Serializable;

/**
 * Classe que representa uma transacao em determinado vertice.
 * 
 * @author Bruno Mourao, Thiago Ripardo.
 * @version 1.0
 */
public class Transacao implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id, status;
	private int TS;
	//private Bloqueio block = new Bloqueio();
	private int infoItem = 0;
	//private ArrayList<Item> 
	/**
	 * Construtor Transacao
	 * @since 1.0 
	 */
	public Transacao(){
		
	}
	
	/**
	 * Construtor Transacao
	 * @param id String
	 * @param status String
	 * @since 1.0 
	 */
	public Transacao(String id, String status,int TS){
		this.id = id;
		setStatus(status);
		setTimestamp(TS);
	}
	
	/**
	 * Retornar id da transacao
	 * @return id String
	 * @since 1.0 
	 */
	public String getID(){
		return this.id;
	}
	
	public void setStatus(String status){
		this.status = status;
	}
	
	/**
	 * Retornar status da transacao
	 * @return status String
	 * @since 1.0 
	 */
	public String getStatus(){
		return this.status;
	}
	
	public int getTimestamp(){
		return this.TS;
	}
	
	public void setTimestamp(int timestamp){
		TS = timestamp;
	}
	
	public void setInfoItem(int infoItem){
		this.infoItem = infoItem;
	}
	
	public int getInfoItem(){
		return this.infoItem;
	}
	
	public boolean equals(Transacao tr){
		if(tr.getID().equalsIgnoreCase(getID()))
			return true;
		return false;
	}
}
