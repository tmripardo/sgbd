package model;

public class Item {
	private String id;
	private int info;
	private Bloqueio bloqueio = new Bloqueio();
	private Wait_Q waitQ = new Wait_Q();
	
	public Item(){}
	
	public Item(String id, int info){
		this.id = id;
		setInfo(info);
		bloqueio.setEstado(Bloqueio.ITEM_NAO_USADO);
	}
	
	public boolean isBloqueado(){
		if((bloqueio.getEstado()==Bloqueio.DESBLOQUEADO) || 
				(bloqueio.getEstado()==Bloqueio.ITEM_NAO_USADO) ||
				(bloqueio.getEstado()==Bloqueio.BLOQUEIO_INVALIDO))
			return false;	
		return true;
	}
	
	public boolean isBloqueadoLeitura(){
		if(bloqueio.getEstado()==Bloqueio.BLOQUEADO_LEITURA)
			return true;
		return false;
	}
	
	public void addBloqueioDeLeitura(Transacao tr){
		
		bloqueio.setTransacaoBloqueante(tr);
		bloqueio.setEstado(Bloqueio.BLOQUEADO_LEITURA);
		bloqueio.setTS(tr.getTimestamp());
	}
	
	public void addBloqueioDeEscrita(Transacao tr){
		
		bloqueio.setTransacaoBloqueante(tr);
		bloqueio.setEstado(Bloqueio.BLOQUEADO_ESCRITA);
		bloqueio.setTS(tr.getTimestamp());
		
	}
	
	public void addNaEspera(Transacao tr, int tipoBloqueio){
		waitQ.inserir(tr, tipoBloqueio);
	}
	
	public void desbloquear(){
		getBloqueio().setTransacaoBloqueante(null);
		getBloqueio().setEstado(Bloqueio.DESBLOQUEADO);
	}
	
	public void setInfo(int info){
		this.info = info;
	}
	
	public String getId(){
		return this.id;
	}
	
	public int getInfo(){
		return this.info;
	}
	public Bloqueio getBloqueio(){
		return this.bloqueio;
	}
	public Wait_Q getWait_Q(){
		return this.waitQ;
	}
	
	public boolean equals(String s){
		if(this.id.equals(s))
			return true;
		return false;
	}
	public void resetWaitQ(){
		this.waitQ = new Wait_Q();
	}
}
