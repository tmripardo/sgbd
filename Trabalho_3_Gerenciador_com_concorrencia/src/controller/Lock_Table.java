package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import model.Item;

public class Lock_Table {
	
	private ArrayList<Item> listaDeItens = new ArrayList<>();
	
	public void addItem(Item item){
		if(!isItemOnTable(item))
			this.listaDeItens.add(item);
		else
			System.out.println("O item já está na tabela!");
	}
	
	public Item getItem(String id) throws NullPointerException{
		Item it = null;
		for (int i = 0; i<this.listaDeItens.size(); i++) {
			if(this.listaDeItens.get(i).equals(id)){
				it = this.listaDeItens.get(i);
				break;
			}
		}
		if(it==null) throw new NullPointerException("NULO");
		else return it;
	}
	
	public boolean isItemOnTable(Item item){
		try{
			for (int i = 0; i<this.listaDeItens.size(); i++) {
				if(item.equals(this.listaDeItens.get(i).getId())){
					System.out.println("isontable");
					return true;
				}
			}
		}catch(NoSuchElementException ee){
			return false;
		}
		return false;
	}
	public List<Item >getlistaDeItens(){
		return listaDeItens;
	}
}
