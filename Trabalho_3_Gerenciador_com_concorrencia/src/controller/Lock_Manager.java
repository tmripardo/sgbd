package controller;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

import model.*;

public class Lock_Manager {
	
	private Lock_Table lockTable = new Lock_Table();

	public void lx(Transacao tr, Item d){
		Transacao tx;
		
		if(lockTable.isItemOnTable(d)){
			try{
				tx = lockTable.getItem(d.getId()).getBloqueio().getTransacaoBloqueante();
			
				if(tx.getTimestamp()>=tr.getTimestamp()){
					Wound_n_Wait(tr,d,tx);
					tr.setInfoItem(d.getInfo());
					//System.out.println("Teste");
				}
				else if(tx.getTimestamp()<tr.getTimestamp()){
				
					lockTable.getItem(d.getId()).addNaEspera(tr, Bloqueio.BLOQUEADO_ESCRITA);
					tr.setStatus("Waiting");
					tr.setInfoItem(0);
					//System.out.println(tr.getStatus());
					//System.out.println(tx.getTimestamp()+" " +tr.getTimestamp());
				
				}
			}
			catch(NullPointerException e){}
		}
		else{
			d.addBloqueioDeLeitura(tr);
			d.getBloqueio().setTransacaoBloqueante(tr);
			lockTable.addItem(d);
			tr.setInfoItem(d.getInfo());
			//tr.setStatus("WRITE");
			//System.out.println("Teste else");
		}
	}
	
	public void ls(Transacao tr, Item d){
		
		if(lockTable.isItemOnTable(d)){
			
			lockTable.getItem(d.getId()).addNaEspera(tr, Bloqueio.BLOQUEADO_LEITURA);
			tr.setStatus("READ");
			tr.setInfoItem(d.getInfo());
			
		}	
		else{
			d.addBloqueioDeLeitura(tr);
			d.getBloqueio().setTransacaoBloqueante(tr);
			lockTable.addItem(d);
			tr.setInfoItem(d.getInfo());
			
		}
	}	

	public void u(Item d){
		lockTable.getItem(d.getId()).desbloquear();
	}
	public void Wound_n_Wait(Transacao tr, Item d, Transacao t2){
		//rollback na t2 e dar o bloqueio para a tr
		if(!tr.equals(t2)){
			
			u(d);
			
			lockTable.getItem(d.getId()).getBloqueio().setTransacaoBloqueante(tr);
			
			if(tr.getStatus().equals("READ")){
				lockTable.getItem(d.getId()).addBloqueioDeLeitura(tr);
			}
			else{
				lockTable.getItem(d.getId()).addBloqueioDeEscrita(tr);
			}

			Tr_Manager.remTransacao(t2.getID());
			t2.setInfoItem(0);
			Tr_Manager.setTransacao(t2.getID(), "TR_Begin", Tr_Manager.getGrafo().getVertice("TR_Iniciada"));
		}
		else{
			
			if(tr.getStatus().equals("READ")){
				
				lockTable.getItem(d.getId()).addBloqueioDeLeitura(tr);
			}
			else{
				
				lockTable.getItem(d.getId()).addBloqueioDeEscrita(tr);
				
				while (true){
					
					try{
						
						Transacao ts;
						ts = lockTable.getItem(d.getId()).getWait_Q().getProximaTransacao();
						
						if(!ts.equals(tr)){
							
							Tr_Manager.remTransacao(ts.getID());
							ts.setInfoItem(0);
							Tr_Manager.setTransacao(ts.getID(), "TR_Begin", Tr_Manager.getGrafo().getVertice("TR_Iniciada"));
							lockTable.getItem(d.getId()).getWait_Q().remover();
						}
						else{
							
							lockTable.getItem(d.getId()).getWait_Q().remover();
						}
					}
					catch(NoSuchElementException ee){
						break;
					}
				}
			}
		}
	}
	public Lock_Table getLockTable(){
		return this.lockTable;
	}
}

