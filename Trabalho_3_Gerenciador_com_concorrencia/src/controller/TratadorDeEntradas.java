package controller;
import java.util.StringTokenizer;
import java.util.ArrayList;
public class TratadorDeEntradas {
	public TratadorDeEntradas(){}	
	public ArrayList<String> getListadeComandos(String historia){
		ArrayList<String> comandos = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(historia, "()");
        String aux;
        while (st.hasMoreTokens()) { 
        	aux = st.nextToken().toString();
        	if(aux.length() == 2){
        		if((aux.charAt(0) == ('r')) || (aux.charAt(0) == ('w'))){ 
        			comandos.add(Character.toString(aux.charAt(0)));
        			comandos.add(Character.toString(aux.charAt(1)));
        		}
        		else{
        			comandos.add(aux);
        		}	
        	}
        	else{		
            	comandos.add(aux);
            }
        }
		return comandos;
	}
}
